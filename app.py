from models.customers import database
from flask import Flask,jsonify,request
import json

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

@app.route("/")
def main():
    return "Welcome Flask..!"
    

@app.route("/users")
def showUsers():
    dbresult = mysqldb.showUsers()
    #print(dbresult)   #akan muncul di CMD atau Terminal 
    result = []
    for items in dbresult:
        user = {
            "userid" : items[0],
            "username" : items[1],
            "firstname" : items[2],
            "lastname" : items[3],
            "email" : items[4]            
        }
        result.append(user)        
    return jsonify(result) #return mengembalikan pada route

@app.route("/users", methods=["POST"])
def showUser():
    params = request.json
    dbresult = mysqldb.showUserById(**params)
    user = {
        "userid" : dbresult[0],
        "username" : dbresult[1],
        "firstname" : dbresult[2],
        "lastname" : dbresult[3],
        "email" : dbresult[4]            
    }        
    return jsonify(user)

@app.route("/updateuser", methods=["POST"])
def updateUser():
    params = request.json
    dbresult = mysqldb.updateUserById(**params)   
    user = {
        "userid" : dbresult[0],
        "username" : dbresult[1],
        "firstname" : dbresult[2],
        "lastname" : dbresult[3],
        "email" : dbresult[4]            
    }        
    return jsonify(user)

@app.route("/insertuser", methods=["POST"])
def insertUser():
    params = request.json
    dbresult = mysqldb.insertUser(**params)   
    user = {
        "userid" : dbresult[0],
        "username" : dbresult[1],
        "firstname" : dbresult[2],
        "lastname" : dbresult[3],
        "email" : dbresult[4]            
    }        
    return jsonify(user)

@app.route("/deleteuser", methods=["POST"])
def deleteUser():
    params = request.json
    dbresult = mysqldb.deleteUserById(**params)       
    return dbresult



if __name__ == "__main__":
    mysqldb = database()   
    if mysqldb.db.is_connected():
        print('Connected to MySQL database')
    app.run(debug=True)    
        
    if mysqldb.db is not None and mysqldb.db.is_connected():
        mysqldb.db.close()
        print("Database Closed")