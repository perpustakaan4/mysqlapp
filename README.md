# mysqlapp

belajarPython

nama : fauzi oktriyandaru
email : fauziokt@gmail.com

membuat REST API

Database : perpustakaan
Tabel : customers
Kolom : userid(int,AI), username(varchar), firstname(varchar), lastname(varchar),email(varchar)

Endpoint REST API :
1. greeting 
localhost:5000/
method : GET
2. melihat semua user
localhost:5000/users
method : GET
3. melihat user tertentu
localhost:5000/users
method:POST
input : JSON
contoh : {"userid":1}
4. mengubah user tertentu
localhost:5000/updateuser
method:POST
input : JSON
contoh : {"userid":1,"username":"fauzi","firstname":"fauzi","lastname":"fauzio","email":"fauziokt@gmail.com"}
5. menambah user
localhost:5000/updateuser
method:POST
input : JSON
contoh : {"username":"fauzi1","firstname":"fauzi1","lastname":"fauzio1","email":"fauziokt1@gmail.com"}
6. menghapus user tertentu
localhost:5000/deleteuser
method:POST
input : JSON
contoh : {"userid":1}

